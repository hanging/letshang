//
//  Constants.swift
//  Lets Hang
//
//  Created by Cameron Long on 10/10/17.
//  Copyright © 2017 Cameron Long. All rights reserved.
//

import Foundation

struct Constants {
    
    struct ViewControllers {
        static let Home = "Home"
        static let Signup = "Signup"
        static let CreateUsername = "CreateUsername"
        static let ProfilePicture = "ProfilePicture"
        static let LeftPanel = "Left"
        static let Groups = "Groups"
        static let Post = "PostVC"
    }
    
    struct Storyboards {
        static let Main = "Main"
        static let Home = "Home"
    }
    
    struct DatabaseNodes {
        static let Users = "users"
        static let Username = "username"
        static let profileImage = "profileImageURL"
    }
}

//
//  HomeHeaderView.swift
//  Lets Hang
//
//  Created by Cameron Long on 11/16/17.
//  Copyright © 2017 Cameron Long. All rights reserved.
//

import Foundation
import UIKit
import FirebaseDatabase
import FirebaseStorage

class HomeHeaderView: UIView {

    //MARK: IBOutlets
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var profilePictureButton: UIButton!
    
    //User instance
    var user: AppUser
    
    override init(frame: CGRect) {
        user = AppUser.getInstance()
        super.init(frame: frame)
        setupView()
        setDate()
    }
    
    required init?(coder aDecoder: NSCoder) {
        user = AppUser.getInstance()
        super.init(coder: aDecoder)
        setupView()
        setDate()
    }
    
    //sets up date for dateLabel
    let timestampFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .long
        dateFormatter.timeStyle = .none
        
        return dateFormatter
    }()
    
    /*
     This function sets the data in the format month, day, year
    */
    private func setDate() -> Void {
        let formattedDate = timestampFormatter.string(from: Date())
        dateLabel.text = formattedDate
    }
    
    // Performs the initial setup.
    private func setupView() {
        let view = viewFromNibForClass()
        view.frame = bounds
        
        // Auto-layout stuff.
        view.autoresizingMask = [
            UIViewAutoresizing.flexibleWidth,
            UIViewAutoresizing.flexibleHeight
        ]
        
        // Show the view.
        addSubview(view)
        
        //Storage ref
        let picRef = Storage.storage().reference(withPath: "\(user.uid!)/\(user.profilePicID!)")
        picRef.getData(maxSize: 1*1024*1024) { (data, error) in
            if let error = error {
                print(error.localizedDescription)
            }
            else {
                
                let image = UIImage(data: data!)
                DispatchQueue.main.async {
                    //make picture circular
                    self.profilePictureButton.formatToCircle()
                    
                    self.profilePictureButton.setImage(image!, for: .normal)
                    
                    
                    //update users profileImage field
                    self.user.profileImage = image
                }
            }
        }
    }
    
    // Loads a XIB file into a view and returns this view.
    private func viewFromNibForClass() -> UIView {
        
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil).first as! UIView
        
        /* Usage for swift < 3.x
         let bundle = NSBundle(forClass: self.dynamicType)
         let nib = UINib(nibName: String(self.dynamicType), bundle: bundle)
         let view = nib.instantiateWithOwner(self, options: nil)[0] as! UIView
         */
        
        return view
    }

}

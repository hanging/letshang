//
//  User.swift
//  Lets Hang
//
//  Created by Cameron Long on 10/10/17.
//  Copyright © 2017 Cameron Long. All rights reserved.
//

import Foundation
import FirebaseDatabase
import FirebaseStorage
import FirebaseAuth
import UIKit

class AppUser: NSObject {
    var username: String?
    var uid: String?
    var profileImage: UIImage?
    var profileImageUrl: String?
    var profilePicID: String?
    
    init(uid: String, username: String) {
        self.uid = uid
        self.username = username
        super.init()
    }
    
    init?(snapshot: DataSnapshot) {
        super.init()
        guard let value = snapshot.value as? NSDictionary else {
            fatalError("problem creating AppUser from data snapshot")
        }
        
        self.uid = (Auth.auth().currentUser?.uid)!
        self.username = value["username"] as? String ?? ""
        self.profileImageUrl = value["profileImageURL"] as? String ?? ""
        self.profilePicID = value["proPicID"] as? String ?? ""
    }
    
    
    private static var currentSingleton: AppUser?
    
    static func getInstance() -> AppUser {
        if let currentUser = currentSingleton {
            return currentUser
        }
        else {
            fatalError("Fatal error, problem with current user")
        }
    }
    
    /*
     This method will set the current user. It will be used in the login view controller.
     */
    static func setCurrent(_ user: AppUser?) {
        currentSingleton = user
    }
}

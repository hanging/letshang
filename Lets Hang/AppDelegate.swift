//
//  AppDelegate.swift
//  Lets Hang
//
//  Created by Cameron Long on 9/25/17.
//  Copyright © 2017 Cameron Long. All rights reserved.
//

import UIKit
import CoreData
import Firebase

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        FirebaseApp.configure()
        configureRootController(window: window)
        return true
    }

    //Method gets called when user is prompted to shut off phone,
    //but can still cancel that shut down prompt.
    func applicationWillResignActive(_ application: UIApplication) {
        
        
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    //Method gets called when users phone dies.
    func applicationWillTerminate(_ application: UIApplication) {
        
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }

    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "Lets_Hang")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }

}

extension AppDelegate {
    func configureRootController(window: UIWindow?) {
        guard let window = window else {
            return
        }
        
        if let user = Auth.auth().currentUser {
            //grab reference to current user in database
            let userDatabaseRef = Database.database().reference()
                .child(Constants.DatabaseNodes.Users)
                .child(user.uid)
            
            //create local instance of user via data snapshot
            userDatabaseRef.observeSingleEvent(of: .value, with: { (snapshot) in
                let localUser = AppUser(snapshot: snapshot)
                AppUser.setCurrent(localUser)
                
                //user is signed in, present home screen
                
                window.rootViewController = ContainerViewController()
                window.makeKeyAndVisible()
            })
        }
        else {
            //user is not signed in, present sign up screen
            let storyboard = UIStoryboard.init(name: Constants.Storyboards.Main, bundle: nil)
            
            guard let initialViewController = storyboard.instantiateInitialViewController() else {
                return
            }
            
            window.rootViewController = initialViewController
            window.makeKeyAndVisible()
        }
    }
}


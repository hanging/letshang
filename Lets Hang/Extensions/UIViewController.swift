//
//  StoryboardInitiation.swift
//  Lets Hang
//
//  Created by Cameron Long on 10/12/17.
//  Copyright © 2017 Cameron Long. All rights reserved.
//
/*
 This extension will be used to configure the initial root view controller.
 It will determine if there is a user signed in already or not. If a user
 is not signed in it will present the sign up view controller but if there
 is an existing user, the home view controller will be presented.
 */

import Foundation
import UIKit

extension UIViewController {
    class func displaySpinner(onView : UIView) -> UIView {
        let spinnerView = UIView.init(frame: onView.bounds)
        spinnerView.backgroundColor = UIColor.init(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.5)
        let ai = UIActivityIndicatorView.init(activityIndicatorStyle: .whiteLarge)
        ai.startAnimating()
        ai.center = spinnerView.center
        
        DispatchQueue.main.async {
            spinnerView.addSubview(ai)
            onView.addSubview(spinnerView)
        }
        
        return spinnerView
    }
    
    class func removeSpinner(spinner :UIView) {
        DispatchQueue.main.async {
            spinner.removeFromSuperview()
        }
    }
    
    enum NavigationState {
        case home
        case groups
    }
}


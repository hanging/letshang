//
//  UIColor.swift
//  Lets Hang
//
//  Created by Cameron Long on 2/24/18.
//  Copyright © 2018 Cameron Long. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    static func letsHangRed() -> UIColor {
        return UIColor(red: 1, green: 46/255, blue: 99/255, alpha: 1)
    }
}

//
//  UIStoryboard.swift
//  Lets Hang
//
//  Created by Cameron Long on 2/10/18.
//  Copyright © 2018 Cameron Long. All rights reserved.
//

import Foundation
import UIKit

extension UIStoryboard {
    
    static func homeStoryboard() -> UIStoryboard { return UIStoryboard(name: Constants.Storyboards.Home, bundle: nil) }
    
    static func leftViewController() -> SidePanelViewController? {
        return homeStoryboard().instantiateViewController(withIdentifier: Constants.ViewControllers.LeftPanel) as? SidePanelViewController
    }
    
    static func homeViewController() -> HomeViewController? {
        return homeStoryboard().instantiateInitialViewController() as? HomeViewController
    }
    
    static func groupsViewController() -> GroupsViewController {
        return homeStoryboard().instantiateViewController(withIdentifier: Constants.ViewControllers.Groups) as! GroupsViewController
    }
    
    static func postViewController() -> PostViewController? {
        return homeStoryboard().instantiateViewController(withIdentifier: Constants.ViewControllers.Post) as? PostViewController
    }
}

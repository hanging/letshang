//
//  UIButton.swift
//  Lets Hang
//
//  Created by Cameron Long on 2/21/18.
//  Copyright © 2018 Cameron Long. All rights reserved.
//

import Foundation
import UIKit

extension UIButton {
    func formatToCircle() {
        self.imageView?.layer.cornerRadius = self.frame.size.width / 2
        self.clipsToBounds = true
    }
}

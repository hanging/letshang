//
//  AuthFlow.swift
//  Lets Hang
//
//  Created by Cameron Long on 10/7/17.
//  Copyright © 2017 Cameron Long. All rights reserved.
/*
 This file is meant to handle all processes related to authentication of a user.
 Methods in thise file will be used in SignupViewcontroller, LoginViewController.
 */

import Foundation
import UIKit
import Firebase

class AuthService {
    
    /*
     Method creates a new user, implemented in signupviewcontroller. Takes in the strings from the email and password textfields
 
    static func createNewUser(stringFromEmail email1: String?, stringFromPassword password1: String?) -> Int {
        guard let email = email1 else {
            delegate?.showAlert(whichAlert: 1)
            return 1
        }
        
        guard let password = password1 else {
            delegate?.showAlert(whichAlert: 1)
            return 1
        }
        
        Auth.auth().createUser(withEmail: email, password: password) { (user, error) in
            if error != nil {
                delegate?.showAlert(whichAlert: 2)
            }
        }
    }
    */
    
    static func forceSignOut(user: User) -> Void {
        AppUser.setCurrent(nil)
        
        do {
            try Auth.auth().signOut()
        }
        catch {
            print(error)
        }
        
        user.delete(completion: { (error) in
            if let err = error {
                print("Error when deleting user. printing localized description: \(err.localizedDescription)")
            }
        })
    }
}

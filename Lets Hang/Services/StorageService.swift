//
//  StorageService.swift
//  Lets Hang
//
//  Created by Cameron Long on 12/9/17.
//  Copyright © 2017 Cameron Long. All rights reserved.
//

import Foundation
import FirebaseDatabase

class StorageService {
    static func retrieveUserProfilePicture(userUID: String, completion: @escaping (Data) -> Void) -> Void {
        let userRef = Database.database().reference()
            .child(Constants.DatabaseNodes.Users)
            .child(userUID)
        
        userRef.observeSingleEvent(of: .value, with: { (snapshot) in
            
            let value = snapshot.value as? NSDictionary
            let urlString = value![Constants.DatabaseNodes.profileImage] as? String ?? ""
            let url = URL(string: urlString)
            let request = URLRequest(url: url!)
            
            let configuration: URLSessionConfiguration = URLSessionConfiguration.default
            let session = URLSession(configuration: configuration)
            
            let dataTask = session.dataTask(with: request, completionHandler: { (data, response, error) in
                if let error = error {
                    print("error in url session: \(error.localizedDescription)")
                    return
                }
                
                completion(data!)
            })
            
            dataTask.resume()
            
        })
    }
    
}

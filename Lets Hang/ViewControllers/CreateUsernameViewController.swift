//
//  CreateUsernameViewController.swift
//  Lets Hang
//
//  Created by Cameron Long on 10/20/17.
//  Copyright © 2017 Cameron Long. All rights reserved.
//

import UIKit
import FirebaseDatabase
import FirebaseAuth

class CreateUsernameViewController: UIViewController {
    
    //MARK: IBOutlets
    @IBOutlet weak var usernameTextfield: UITextField!
    
    //MARK: IBActions
    @IBAction func nextButtonTapped(_ sender: UIButton) {
        //check that usernameTextfield contains text
        if (usernameTextfield.text == nil || usernameTextfield.text == "" ) {
            let alert = UIAlertController(title: "Error",
                                          message: "There was an error when creating your username",
                                          preferredStyle: .alert)
            let action = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(action)
            self.present(alert, animated: true, completion: nil)
        }
        else {
            
            //get current user after sign up
            let FIRuser = Auth.auth().currentUser
            guard let user = FIRuser else {
                return
            }
        
            //set singleton to appUser
            AppUser.setCurrent(AppUser(uid: user.uid, username: usernameTextfield.text!))
            let appUser = AppUser.getInstance()
            
            let ref = Database.database().reference()
            let usersRef = ref.child(Constants.DatabaseNodes.Users)
            let uidRef = usersRef.child(appUser.uid!)
            uidRef.setValue([Constants.DatabaseNodes.Username: usernameTextfield.text!])
            
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: Constants.ViewControllers.ProfilePicture) as! ProfilePictureViewController
            self.navigationController?.pushViewController(vc, animated: true)
            
            
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}

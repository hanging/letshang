//
//  SidePanelViewController.swift
//  Lets Hang
//
//  Created by Cameron Long on 2/10/18.
//  Copyright © 2018 Cameron Long. All rights reserved.
//

import UIKit

class SidePanelViewController: UIViewController {

    var delegate: HomeViewControllerDelegate?
    var user: AppUser!
    
    //MARK: IBOutlets
    @IBOutlet weak var profileImageButton: UIButton!
    @IBOutlet weak var homePicture: UIImageView!
    @IBOutlet weak var homeLabel: UILabel!
    @IBOutlet weak var homeTapView: UIView!
    @IBOutlet weak var groupsTapView: UIView!
    @IBOutlet weak var groupsLabel: UILabel!
    @IBOutlet weak var groupsIcon: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        user = AppUser.getInstance()
        
        if user.profileImage != nil {
            profileImageButton.setImage(user.profileImage, for: .normal)
            profileImageButton.formatToCircle()
        }
        
        homeTapView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(homeSelected)))
        groupsTapView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(groupsSelected)))
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func homeSelected() {
        delegate?.switchLeftPanel(state: .home)
    }
    
    @objc func groupsSelected() {
        delegate?.switchLeftPanel(state: .groups)
    }

}

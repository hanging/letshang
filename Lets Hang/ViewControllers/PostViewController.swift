//
//  PostViewController.swift
//  Lets Hang
//
//  Created by Cameron Long on 3/28/18.
//  Copyright © 2018 Cameron Long. All rights reserved.
//

import UIKit

class PostViewController: UIViewController {

    @IBOutlet weak var containerView: UIView!
    var textViewHeight: CGFloat!
    private enum LateNightButtonState {
        case selected
        case notSelected
    }
    
    private enum EveningButtonState {
        case selected
        case notSelected
    }
    //variables for button states
    private var lateNightState: LateNightButtonState = .notSelected
    private var eveningState: EveningButtonState = .notSelected
    
    //Text Views
    private var lateNightTextView: UITextView?
    private var eveningTextView: UITextView?
    private var afternoonTextView: UITextView?
    private var morningTextView: UITextView?
    
    //MARK: Time period views
    @IBOutlet weak var morningView: UIView!
    @IBOutlet weak var afternoonView: UIView!
    @IBOutlet weak var eveningView: UIView!
    @IBOutlet weak var lateNightView: UIView!
    
    //Constraints
    @IBOutlet weak var morningBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var afternoonBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var eveningBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var lateNightBottomConstraint: NSLayoutConstraint!
    
    //UIButtons
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var postButton: UIButton!
    @IBOutlet weak var lateNightButton: UIButton!
    @IBOutlet weak var eveningButton: UIButton!
    @IBOutlet weak var afternoonButton: UIButton!
    @IBOutlet weak var morningButton: UIButton!
    
    
    //MARK: IBActions
    
    @IBAction func lateNightButtonTapped(_ sender: UIButton) {
        let origYPos = self.view.bounds.height - (self.containerView.bounds.height - self.lateNightView.frame.maxY)
        if lateNightState == .notSelected {
            lateNightButton.setImage(UIImage(named: "highlighteddone"), for: .normal)
            lateNightState = .selected
            
            self.lateNightTextView = UITextView(frame: CGRect(x: 0, y: origYPos, width: self.view.bounds.width, height: 0))
            self.view.addSubview(self.lateNightTextView!)
            
            self.view.layoutIfNeeded()
            UIView.animate(withDuration: 1, delay: 0, options: .curveEaseInOut, animations: {
                self.lateNightBottomConstraint.constant += self.textViewHeight
                self.lateNightTextView?.frame = CGRect(x: 0, y: origYPos - self.textViewHeight, width: self.view.bounds.width, height: self.textViewHeight)
                self.view.layoutIfNeeded()
            }, completion: nil)
        }
        else {
            lateNightButton.setImage(UIImage(named: "tpcircle"), for: .normal)
            lateNightState = .notSelected
            self.view.layoutIfNeeded()
            UIView.animate(withDuration: 1, delay: 0, options: .curveEaseInOut, animations: {
                self.lateNightBottomConstraint.constant -= self.textViewHeight
                self.view.layoutIfNeeded()
            }, completion: {(bool) in
                self.lateNightTextView?.isHidden = true
                self.lateNightTextView?.removeFromSuperview()
                self.lateNightTextView = nil
            })
        }
    }
    
    @IBAction func eveningButtonTapped(_ sender: UIButton) {
        if eveningState == .notSelected {
            eveningButton.setImage(UIImage(named: "highlighteddone"), for: .normal)
            eveningState = .selected
            
            let origYPos = self.view.bounds.height - (self.containerView.bounds.height - self.eveningView.frame.maxY)
            self.eveningTextView = UITextView(frame: CGRect(x: 0, y: origYPos, width: self.view.bounds.width, height: 0))
            self.view.addSubview(self.eveningTextView!)
            
            self.view.layoutIfNeeded()
            UIView.animate(withDuration: 1, delay: 0, options: .curveEaseInOut, animations: {
                self.eveningBottomConstraint.constant += self.textViewHeight
                self.eveningTextView?.frame = CGRect(x: 0, y: origYPos - self.textViewHeight, width: self.view.bounds.width, height: self.textViewHeight)
                self.view.layoutIfNeeded()
            }, completion: { (bool) in
                let topConstraint = NSLayoutConstraint(item: self.eveningTextView!, attribute: .top, relatedBy: .equal, toItem: self.eveningView, attribute: .bottom, multiplier: 1.0, constant: 0)
                let bottomConstraint = NSLayoutConstraint(item: self.eveningTextView!, attribute: .bottom, relatedBy: .equal, toItem: self.lateNightView, attribute: .top, multiplier: 1.0, constant: 0)
                NSLayoutConstraint.activate([bottomConstraint, topConstraint])
            })
        }
        else {
            eveningButton.setImage(UIImage(named: "tpcircle"), for: .normal)
            eveningState = .notSelected
            self.view.layoutIfNeeded()
            UIView.animate(withDuration: 1, delay: 0, options: .curveEaseInOut, animations: {
                self.eveningBottomConstraint.constant -= self.textViewHeight
                self.eveningTextView?.center.y += self.textViewHeight
                self.view.layoutIfNeeded()
            }, completion: {(bool) in
                self.eveningTextView?.isHidden = true
                self.eveningTextView?.removeFromSuperview()
                self.eveningTextView = nil
            })
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = UIColor.clear
        view.isOpaque = false
        
        textViewHeight = (self.view.bounds.height - self.containerView.bounds.height) / 4
        setUpButtons()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func setUpButtons() {
        
        //Add top border to cancelButton
        let border = CALayer()
        border.backgroundColor = UIColor.white.cgColor
        border.frame = CGRect(x: 0,y: 0, width: postButton.frame.size.width, height: 1)
        cancelButton.layer.addSublayer(border)
        
        //Add top border to postButton
        let border2 = CALayer()
        border2.backgroundColor = UIColor.white.cgColor
        border2.frame = CGRect(x: 0,y: 0, width: postButton.frame.size.width, height: 1)
        postButton.layer.addSublayer(border2)
        
        //Add border to split buttons
        let border3 = CALayer()
        border3.backgroundColor = UIColor.white.cgColor
        border3.frame = CGRect(x: cancelButton.frame.size.width - 1, y: 0, width: 1, height: cancelButton.frame.size.height)
        cancelButton.layer.addSublayer(border3)
        
        let border4 = CALayer()
        border4.backgroundColor = UIColor.white.cgColor
        border4.frame = CGRect(x: 0, y: 0, width: 1, height: cancelButton.frame.size.height)
        postButton.layer.addSublayer(border4)
    }
    
    /*private func animateFor(timePeriod tp: UIView, periodButton pb: UIButton, buttonState bState: ButtonStates, textView tv: inout UITextView, constraint: NSLayoutConstraint) {
        if bState == .notSelected {
            pb.setImage(UIImage(named: "highlighteddone"), for: .normal)
            bState = .selected
            
            let origYPos = self.view.bounds.height - (self.containerView.bounds.height - tp.frame.maxY)
            tv = UITextView(frame: CGRect(x: 0, y: origYPos, width: self.view.bounds.width, height: 0))
            self.view.addSubview(tv)
            
            let closure = {
                constraint.constant += self.textViewHeight
                tv.frame = CGRect(x: 0, y: origYPos - self.textViewHeight, width: self.view.bounds.width, height: self.self.textViewHeight)
                self.view.layoutIfNeeded()
            }
            self.view.layoutIfNeeded()
            UIView.animate(withDuration: 1, delay: 0, options: .curveEaseInOut, animations: {
                constraint.constant += self.textViewHeight
                tv.frame = CGRect(x: 0, y: origYPos - self.textViewHeight, width: self.view.bounds.width, height: self.self.textViewHeight)
                self.view.layoutIfNeeded()
            }, completion: nil)
        }
        else {
            lateNightButton.setImage(UIImage(named: "tpcircle"), for: .normal)
            lateNightState = .notSelected
            self.view.layoutIfNeeded()
            UIView.animate(withDuration: 1, delay: 0, options: .curveEaseInOut, animations: {
                self.lateNightBottomConstraint.constant -= 150
                self.view.layoutIfNeeded()
            }, completion: {(bool) in
                self.lateNightTextView?.isHidden = true
                self.lateNightTextView?.removeFromSuperview()
                self.lateNightTextView = nil
            })
        }
    }*/

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

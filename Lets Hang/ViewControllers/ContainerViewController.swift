//
//  ContainerViewController.swift
//  Lets Hang
//
//  Created by Cameron Long on 2/10/18.
//  Copyright © 2018 Cameron Long. All rights reserved.
//

import UIKit

class ContainerViewController: UIViewController {
    enum PanelState {
        case panelExpanded
        case panelCollapsed
    }

    //MARK: instance vars
    
    var leftViewController: SidePanelViewController?
    let centerPanelExpandedOffset: CGFloat = 60
    var currentOpenController: NavigationState = .home
    var currentNavigationController: UINavigationController!
    var currentState: PanelState = .panelCollapsed {
        didSet {
            let shouldShowShadow = (currentState != .panelCollapsed)
            showShadowForCenterViewController(shouldShowShadow)
        }
    }
    
    private lazy var groupsNavigationController: UINavigationController = {
        let gvc = UIStoryboard.groupsViewController()
        gvc.delegate = self
        let gnvc = UINavigationController(rootViewController: gvc)
        
        addChildViewController(gnvc)
        view.addSubview(gnvc.view)
        gnvc.didMove(toParentViewController: self)
        
        //set up swipe gesture
        let panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(handlePanGesture(_:)))
        gnvc.view.addGestureRecognizer(panGestureRecognizer)
        return gnvc
    }()
    
    private lazy var homeNavigationController: UINavigationController = {
        let hvc = UIStoryboard.homeViewController()!
        hvc.delegate = self
        let hnvc = UINavigationController(rootViewController: hvc)
        
        let navBar = hnvc.navigationBar
        navBar.isTranslucent = false
        navBar.barTintColor = UIColor.letsHangRed()
        
        addChildViewController(hnvc)
        view.addSubview(hnvc.view)
        hnvc.didMove(toParentViewController: self)
        
        //set up swipe gesture
        let panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(handlePanGesture(_:)))
        hnvc.view.addGestureRecognizer(panGestureRecognizer)
        return hnvc
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        currentNavigationController = homeNavigationController
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}

extension ContainerViewController: HomeViewControllerDelegate {
    /*
     To be used by delegate in HomeViewController
     */
    func changeCurrentViewController() {
        currentOpenController = .home
    }
    
    func collapsePanel() {
        switch currentState {
        case .panelExpanded:
            //Navigation State arbitrarily set to home. Doesn't matter bc leftPanelViewController will be set to nil
            switchLeftPanel(state: NavigationState.home)
        default:
            break
        }
    }
    
    func switchLeftPanel(state: NavigationState) {
        determineCurrentNavigationController(newState: state)
        currentOpenController = state
        let notAlreadyExpanded = (currentState != .panelExpanded)
        
        if notAlreadyExpanded {
            addPanelViewController()
        }
        
        animatePanel(shouldExpand: notAlreadyExpanded)
    }
    
    func determineCurrentNavigationController(newState: NavigationState) {
        if newState != currentOpenController {
            //In order to determine what the past view controller was in order to take it out of the view hierarchy.
            switch currentOpenController {
            case .groups:
                groupsNavigationController.willMove(toParentViewController: nil)
                groupsNavigationController.view.removeFromSuperview()
                groupsNavigationController.removeFromParentViewController()
            case .home:
                homeNavigationController.willMove(toParentViewController: nil)
                homeNavigationController.view.removeFromSuperview()
                homeNavigationController.removeFromParentViewController()
            }
            
            //set currentNavigationController to the controller to be shown.
            switch newState {
            case .groups:
                addChildViewController(groupsNavigationController)
                view.addSubview(groupsNavigationController.view)
                groupsNavigationController.didMove(toParentViewController: self)
                currentNavigationController = groupsNavigationController
            case .home:
                addChildViewController(homeNavigationController)
                view.addSubview(homeNavigationController.view)
                homeNavigationController.didMove(toParentViewController: self)
                currentNavigationController = homeNavigationController
            }
        }
    }
    
    /*
     Determine which picture and label should be highlighted depending on
     which view controller triggered the side panel
     */
    func highlightView() {
        switch currentOpenController {
        case .home:
            leftViewController?.homeLabel.textColor = UIColor.letsHangRed()
            leftViewController?.homePicture.isHighlighted = true
        case .groups:
            leftViewController?.groupsLabel.textColor = UIColor.letsHangRed()
            leftViewController?.groupsIcon.isHighlighted = true
        }
    }
    
    func addPanelViewController() {
        guard leftViewController == nil else { fatalError("no panel view")}
        
        if let vc = UIStoryboard.leftViewController() {
            addChildSidePanelController(vc)
            leftViewController = vc
            leftViewController?.delegate = self
        }
    }
    
    func addChildSidePanelController(_ sidePanelController: SidePanelViewController) {
        
        //inserts sidePanelController underneath homeViewController
        view.insertSubview(sidePanelController.view, at: 0)
        
        addChildViewController(sidePanelController)
        sidePanelController.didMove(toParentViewController: self)
    }
    
    func animatePanel(shouldExpand: Bool) {
        if shouldExpand {
            highlightView()
            currentState = .panelExpanded
            animateCenterPanelXPosition(
                targetPosition: currentNavigationController.view.frame.width - centerPanelExpandedOffset)
            
        } else {
            animateCenterPanelXPosition(targetPosition: 0) { finished in
                self.currentState = .panelCollapsed
                self.leftViewController?.view.removeFromSuperview()
                self.leftViewController = nil
            }
        }
    }
    
    func animateCenterPanelXPosition(targetPosition: CGFloat, completion: ((Bool) -> Void)? = nil) {
        
        UIView.animate(withDuration: 0.5,
                       delay: 0,
                       usingSpringWithDamping: 0.9,
                       initialSpringVelocity: 0,
                       options: .curveEaseInOut, animations: {
                        self.currentNavigationController.view.frame.origin.x = targetPosition
        }, completion: completion)
    }
    
    func showShadowForCenterViewController(_ shouldShowShadow: Bool) {
        
        if shouldShowShadow {
            currentNavigationController.view.layer.shadowOpacity = 0.8
        } else {
            currentNavigationController.view.layer.shadowOpacity = 0.0
        }
    }
}

extension ContainerViewController: UIGestureRecognizerDelegate {
    @objc func handlePanGesture(_ recognizer: UIPanGestureRecognizer) {
        let gestureIsDraggingFromLeftToRight = (recognizer.velocity(in: view).x > 0)
        
        switch recognizer.state {
            
        case .began:
            if currentState == .panelCollapsed {
                if gestureIsDraggingFromLeftToRight {
                    addPanelViewController()
                }
                
                showShadowForCenterViewController(true)
            }
            
        case .changed:
            if let rview = recognizer.view {
                rview.center.x = rview.center.x + recognizer.translation(in: view).x
                recognizer.setTranslation(CGPoint.zero, in: view)
            }
            
        case .ended:
            if let _ = leftViewController,
                let rview = recognizer.view {
                // animate the side panel open or closed based on whether the view
                // has moved more or less than halfway
                let hasMovedGreaterThanHalfway = rview.center.x > view.bounds.size.width
                animatePanel(shouldExpand: hasMovedGreaterThanHalfway)
                
            }
            
        default:
            break
        }
    }
}

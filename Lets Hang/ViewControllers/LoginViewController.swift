//
//  LoginViewController.swift
//  Lets Hang
//
//  Created by Cameron Long on 10/5/17.
//  Copyright © 2017 Cameron Long. All rights reserved.
//

import UIKit
import FirebaseDatabase
import FirebaseAuth

class LoginViewController: UIViewController {

    //MARK: IBOutlets
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    //MARK: IBActions
    @IBAction func unwindFromResetPassword(unwindSegue: UIStoryboardSegue) {
    
    }
    
    @IBAction func logInButtonPressed(_ sender: UIButton) {
        if (self.emailTextField.text == "" || self.passwordTextField.text == "") {
            
            //Alert to tell the user that there was an error because they didn't fill anything in the textfields because they didn't fill anything in
            
            let alertController = UIAlertController(title: "Error", message: "Please enter an email and password.", preferredStyle: .alert)
            
            let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alertController.addAction(defaultAction)
            
            self.present(alertController, animated: true, completion: nil)
            
        } else {
            
            Auth.auth().signIn(withEmail: self.emailTextField.text!, password: self.passwordTextField.text!) { (user, error) in
                
                if error == nil {
                    
                    guard let user = user else {
                        fatalError("problem siging in user in loginViewController")
                    }
                    
                    let spinnerView = UIViewController.displaySpinner(onView: self.view)
                    
                    //Grab user information from database
                    let userRef = Database.database().reference(withPath: "\(Constants.DatabaseNodes.Users)/\(user.uid)")
                    
                    userRef.observeSingleEvent(of: .value, with: { (snapshot) in
                        let user = AppUser(snapshot: snapshot)
                        guard let appUser = user else {
                            fatalError("Problem creating local user via data snapshot")
                        }
                        AppUser.setCurrent(appUser)
                        
                        UIViewController.removeSpinner(spinner: spinnerView)
                        DispatchQueue.main.async {
                            let sb = UIStoryboard(name: Constants.Storyboards.Home, bundle: nil)
                            self.view.window?.rootViewController = sb.instantiateInitialViewController()
                            self.view.window?.makeKeyAndVisible()
                        }
                    })
                    
                } else {
                    
                    //Tells the user that there is an error and then gets firebase to tell them the error
                    let alertController = UIAlertController(title: "Error", message: error?.localizedDescription, preferredStyle: .alert)
                    
                    let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                    alertController.addAction(defaultAction)
                    
                    self.present(alertController, animated: true, completion: nil)
                }
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

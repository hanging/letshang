//
//  ProfilePictureViewController.swift
//  Lets Hang
//
//  Created by Cameron Long on 10/23/17.
//  Copyright © 2017 Cameron Long. All rights reserved.
//

import UIKit
import FirebaseStorage
import FirebaseDatabase
import FirebaseAuth

class ProfilePictureViewController: UIViewController {
    
    var appUser: AppUser?
    
    //MARK: IBOutlets
    @IBOutlet weak var pickImageButton: UIButton!
    
    //MARK: IBActions
    /*function should trigger an alert controller to be presented so the user
     can choose between their photo library or camera to grab a pic for their profile pic*/
    @IBAction func pickPictureButtonTapped(_ sender: UIButton) {
        
        let imagePicker:UIImagePickerController = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.allowsEditing = true
        
        let alert = UIAlertController(title: "Photo Source", message: "Choose a source", preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "PhotoLibrary", style: .default, handler: { (action: UIAlertAction) in
            imagePicker.sourceType = .photoLibrary
            self.present(imagePicker, animated: true, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action: UIAlertAction) in
            //check if camera is available 
            guard UIImagePickerController.isSourceTypeAvailable(.camera) else {
                print("Camera not available")
                return
            }
            
            imagePicker.sourceType = .camera
            self.present(imagePicker, animated: true, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    /*function triggered when user wants to move to home screen.
    should check that the user chose a profile picture. If the
    user did choose a profile picture, home screen should be brought up,
     an alert otherwise.
     */
    @IBAction func finishButtonTapped(_ sender: UIButton) {
        guard (!pickImageButton.currentImage!.isEqual(UIImage(named: "add pic"))) else {
            let alert = UIAlertController(title: "No photo chosen", message: "Tap to create your profile picture!", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
            return
        }
        
        //grab a unique string that is gaurenteed to be different everytime
        let uniqueID = NSUUID().uuidString
        
        guard let appUser = appUser else {
            return
        }
        
        //set local users profileImage variable to image chosen
        appUser.profileImage = pickImageButton.currentImage!
        
        //proPicID: uniqueID
        Database.database().reference()
        .child(Constants.DatabaseNodes.Users)
        .child(appUser.uid!)
        .child("proPicID")
        .setValue("\(uniqueID).jpg")
        
        //create reference in FirebaseStorage
        let storageRef = Storage.storage().reference().child(appUser.uid!).child("\(uniqueID).jpg")
        //populate profilePicID in local user instance
        appUser.profilePicID = "\(uniqueID).jpg"
    
        guard let dataToUpload = UIImageJPEGRepresentation(pickImageButton.currentImage!, 0.8) else {
            print("error converting image to JPEG")
            return
        }
        
        //metadata
        let metadata = StorageMetadata()
        metadata.contentType = "image/jpeg"
        
        storageRef.putData(dataToUpload, metadata: metadata) { (metadata, error) in
            if let err = error {
                print(err)
                fatalError("problem storing picture in profilepictureviewcontroller")
            }
            
            let databaseRef = Database.database().reference()
            guard let urlString = metadata?.downloadURL()?.absoluteString else {
                fatalError("Problem getting absolute string of download URL")
            }
            
            databaseRef.child(Constants.DatabaseNodes.Users).child(appUser.uid!)
            .child(Constants.DatabaseNodes.profileImage).setValue(urlString)
            
            DispatchQueue.main.async {
                self.view.window?.rootViewController = ContainerViewController()
                self.view.window?.makeKeyAndVisible()
            }
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        appUser = AppUser.getInstance()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}

extension ProfilePictureViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    //function used to grab selected photo
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let editedImage = info[UIImagePickerControllerEditedImage] as? UIImage {
            pickImageButton.setImage(editedImage, for: .normal)
        }
        else if let originalImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
        
            //change the image of the button to display the image
            pickImageButton.setImage(originalImage, for: .normal)
        }
        pickImageButton.formatToCircle()
        
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
}

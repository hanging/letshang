//
//  GroupsViewController.swift
//  Lets Hang
//
//  Created by Cameron Long on 2/26/18.
//  Copyright © 2018 Cameron Long. All rights reserved.
//

import UIKit

class GroupsViewController: UIViewController {

    var delegate: HomeViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupNavigationBar()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func setupNavigationBar() {
        let navItem = self.navigationItem
        navItem.title = "Groups"
        
        let rightBarButton = UIBarButtonItem(image: #imageLiteral(resourceName: "post"), style: .plain, target: self, action: #selector(postButtonTapped))
        let leftBarButton = UIBarButtonItem(image: #imageLiteral(resourceName: "menu"), style: .plain, target: self, action: #selector(menuButtonTapped))
        rightBarButton.tintColor = UIColor.white
        leftBarButton.tintColor = UIColor.white
        rightBarButton.imageInsets = UIEdgeInsets(top: -1, left: 0, bottom: -9, right: 0)
        leftBarButton.imageInsets = UIEdgeInsets(top: -1, left: 0, bottom: -9, right: 0)
        
        navItem.rightBarButtonItem = rightBarButton
        navItem.leftBarButtonItem = leftBarButton
    }
    
    @objc func postButtonTapped() {
        print("not implemented yet")
    }
    
    @objc func menuButtonTapped() {
        delegate?.switchLeftPanel(state: .groups)
    }

}

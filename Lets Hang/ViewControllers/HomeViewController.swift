//
//  HomeViewController.swift
//  Lets Hang
//
//  Created by Cameron Long on 10/9/17.
//  Copyright © 2017 Cameron Long. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {

    //MARK: IBOutlets
    @IBOutlet weak var homeTableView: UITableView!
    
    var delegate: HomeViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Set for side panel to know which view controller precedes it
        delegate?.changeCurrentViewController()
        
        homeTableView.dataSource = self
        homeTableView.delegate = self
        
        setUpBarButtons()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func menuButtonTapped(_ sender: Any) {
        delegate?.switchLeftPanel(state: NavigationState.home)
    }
    
    @objc func postButtonTapped(_ sender: Any) {
        let pvcOption = UIStoryboard.postViewController()
        guard let pvc = pvcOption else { fatalError("error creating post view controller")}
        pvc.modalPresentationStyle = .overCurrentContext
        self.present(pvc, animated: true, completion: nil)
    }
    
    func setUpBarButtons() {
        let navItem = self.navigationItem
        navItem.title = "Lets Hang"
        
        let rightBarButton = UIBarButtonItem(image: #imageLiteral(resourceName: "post"), style: .plain, target: self, action: #selector(HomeViewController.postButtonTapped(_:)))
        let leftBarButton = UIBarButtonItem(image: #imageLiteral(resourceName: "menu"), style: .plain, target: self, action: #selector(HomeViewController.menuButtonTapped(_:)))
        rightBarButton.tintColor = UIColor.white
        leftBarButton.tintColor = UIColor.white
        rightBarButton.imageInsets = UIEdgeInsets(top: -1, left: 0, bottom: -9, right: 0)
        leftBarButton.imageInsets = UIEdgeInsets(top: -1, left: 0, bottom: -9, right: 0)
        
        navItem.rightBarButtonItem = rightBarButton
        navItem.leftBarButtonItem = leftBarButton
    }

}

/*
 extension for UITableViewDelegate
 */
extension HomeViewController: UITableViewDelegate {
    public func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 300
    }
    
    public func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return HomeHeaderView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 300))
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 140
    }
}

/*
 extension for UITableViewDataSource
 */
extension HomeViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HomeTableViewCell", for: indexPath)
        //configure cell
        return cell
    }
    
    
}


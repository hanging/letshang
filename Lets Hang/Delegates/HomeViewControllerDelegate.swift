//
//  HomeViewControllerDelegate.swift
//  Lets Hang
//
//  Created by Cameron Long on 2/10/18.
//  Copyright © 2018 Cameron Long. All rights reserved.
//

import Foundation
import UIKit

protocol HomeViewControllerDelegate {
    func switchLeftPanel(state: UIViewController.NavigationState)
    func collapsePanel()
    func changeCurrentViewController()
}

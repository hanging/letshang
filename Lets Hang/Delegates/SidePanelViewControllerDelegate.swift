//
//  SidePanelViewControllerDelegate.swift
//  Lets Hang
//
//  Created by Cameron Long on 2/10/18.
//  Copyright © 2018 Cameron Long. All rights reserved.
//

import Foundation

protocol SidePanelViewControllerDelegate {
    func homeSelected()
    func groupsSelected()
}
